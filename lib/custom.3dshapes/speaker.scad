$fn=50;

translate([0, 0, 0]) {
    cylinder(h=3, d=10, center=true);
}

translate([0,0,1.5]) {
    cylinder(h=3, d=14, center=true);
}

translate([0,0,2]) {
    difference() {
        cylinder(h=3, d=15, center=true);
        union() {
            r=5;
            for(a= [0:45:360]) {
                translate([r*sin(a), r*cos(a), 0]) {
                    cylinder(h=3.1, d=3, center=true);
                }
            }
        }
    }
}

translate([0, 0, 3.5]) {
    color("black", 0.5) {
        cylinder(h=0.1, d=15, center=true);
    }
}