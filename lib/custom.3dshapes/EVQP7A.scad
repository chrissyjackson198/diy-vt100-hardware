BODY_L = 3.5;
BODY_W = 2.9;
BODY_H = 1.35;

HEAD_L = 1.7;
HEAD_W = 0.65;
HEAD_H = 1.1;

PAD_L = 0.6;
PAD_W = 0.62;
PAD_H = 0.16875;
PAD_PITCH = 1.48;

// Body
color("Silver")
cube([BODY_L, BODY_W, BODY_H]);

// Head (Push)
color("DimGray")
translate([BODY_L/2, BODY_W + (HEAD_W/2), BODY_H/2])
cube([HEAD_L, HEAD_W, HEAD_H], center=true);

// Solder Pads
color("Gray") {
    // Bottom Left
    translate([-PAD_L, (BODY_W - PAD_PITCH - PAD_W)/2, 0])
    cube([PAD_L, PAD_W, PAD_H]);
    
    // Top Left
    translate([-PAD_L, (BODY_W + PAD_PITCH - PAD_W)/2, 0])
    cube([PAD_L, PAD_W, PAD_H]);
    
    // Bottom Right
    translate([BODY_L, (BODY_W - PAD_PITCH - PAD_W)/2, 0])
    cube([PAD_L, PAD_W, PAD_H]);
    
    // Top Right
    translate([BODY_L, (BODY_W + PAD_PITCH - PAD_W)/2, 0])
    cube([PAD_L, PAD_W, PAD_H]);
}