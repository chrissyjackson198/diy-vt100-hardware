EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:XPT2046
LIBS:NCS2211
LIBS:SSM3J328RLF
LIBS:IP4220CZ6
LIBS:LD3985M3xR
LIBS:RT9728Bx
LIBS:DIN_6
LIBS:5INCH_DPI_LCD
LIBS:RT4533
LIBS:STM32F767VxTx
LIBS:switches
LIBS:Mounting_Hole
LIBS:diy-vt100-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title "diy-VT100"
Date ""
Rev "0.4"
Comp "Mad Resistor"
Comment1 "https://www.madresistor.com/diy-vt100"
Comment2 "Copyright (C) 2016, 2017 Mad Resistor"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AT24CS01-SSHM U10
U 1 1 58AFAC43
P 5200 2750
F 0 "U10" H 5000 3000 50  0000 C CNN
F 1 "AT24C" H 5650 2450 50  0000 C CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5200 2750 50  0001 C CIN
F 3 "" H 5200 2750 50  0000 C CNN
	1    5200 2750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 58AFAC4A
P 5200 3150
F 0 "#PWR016" H 5200 2900 50  0001 C CNN
F 1 "GND" H 5200 3000 50  0000 C CNN
F 2 "" H 5200 3150 50  0000 C CNN
F 3 "" H 5200 3150 50  0000 C CNN
	1    5200 3150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR017
U 1 1 58AFAC50
P 4400 2850
F 0 "#PWR017" H 4400 2600 50  0001 C CNN
F 1 "GND" H 4400 2700 50  0000 C CNN
F 2 "" H 4400 2850 50  0000 C CNN
F 3 "" H 4400 2850 50  0000 C CNN
	1    4400 2850
	1    0    0    -1  
$EndComp
$Comp
L R R13
U 1 1 58AFAC58
P 3750 2550
F 0 "R13" V 3830 2550 50  0000 C CNN
F 1 "4.7K" V 3750 2550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3680 2550 50  0001 C CNN
F 3 "" H 3750 2550 50  0000 C CNN
	1    3750 2550
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 58AFAC5F
P 3550 2550
F 0 "R12" V 3630 2550 50  0000 C CNN
F 1 "4.7K" V 3550 2550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3480 2550 50  0001 C CNN
F 3 "" H 3550 2550 50  0000 C CNN
	1    3550 2550
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 58AFAC66
P 3350 2550
F 0 "R11" V 3430 2550 50  0000 C CNN
F 1 "10K" V 3350 2550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 3280 2550 50  0001 C CNN
F 3 "" H 3350 2550 50  0000 C CNN
	1    3350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2600 4400 2600
Wire Wire Line
	4400 2600 4400 2850
Wire Wire Line
	4800 2700 4400 2700
Connection ~ 4400 2700
Wire Wire Line
	4800 2800 4400 2800
Connection ~ 4400 2800
Wire Wire Line
	5600 2800 5800 2800
Wire Wire Line
	5600 2650 5800 2650
Wire Wire Line
	5200 2300 5200 2400
Wire Wire Line
	3350 2400 3350 2300
Wire Wire Line
	3750 2300 3750 2400
Connection ~ 3550 2300
Wire Wire Line
	3750 2700 3750 2950
Wire Wire Line
	3550 2700 3550 2950
Wire Wire Line
	3350 2700 3350 2950
Wire Wire Line
	3550 2300 3550 2400
Wire Wire Line
	4800 2950 4700 2950
$Comp
L VDD #PWR018
U 1 1 58AFAC99
P 3550 2300
F 0 "#PWR018" H 3550 2150 50  0001 C CNN
F 1 "VDD" H 3550 2450 50  0000 C CNN
F 2 "" H 3550 2300 50  0000 C CNN
F 3 "" H 3550 2300 50  0000 C CNN
	1    3550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 2300 3750 2300
$Comp
L VDD #PWR019
U 1 1 58AFACD7
P 5200 2300
F 0 "#PWR019" H 5200 2150 50  0001 C CNN
F 1 "VDD" H 5200 2450 50  0000 C CNN
F 2 "" H 5200 2300 50  0000 C CNN
F 3 "" H 5200 2300 50  0000 C CNN
	1    5200 2300
	1    0    0    -1  
$EndComp
Text Notes 4500 4350 0    60   ~ 0
[pin compatible]\nCAT24CS, AT24CS\n\nVT100 need atleast 328bit\n80bit = Tab stop\n168bit = Answer back message (21 char)\n16bit = SETUP B bits\n16bit = UART Speed (4byte)\n8bit = Dimming parameter
Text HLabel 5800 2650 2    60   BiDi ~ 0
SDA
Text HLabel 5800 2800 2    60   Input ~ 0
SCL
Text HLabel 3550 2950 3    60   Input ~ 0
SCL
Text HLabel 3750 2950 3    60   Input ~ 0
SDA
Text HLabel 4700 2950 0    60   Input ~ 0
WP
Text HLabel 3350 2950 3    60   Input ~ 0
WP
$Comp
L TEST TP7
U 1 1 58B27F69
P 2850 2800
F 0 "TP7" H 2850 3100 50  0000 C BNN
F 1 "TEST" H 2850 3050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 2850 2800 50  0001 C CNN
F 3 "" H 2850 2800 50  0000 C CNN
	1    2850 2800
	1    0    0    -1  
$EndComp
$Comp
L TEST TP8
U 1 1 58B28303
P 3050 2800
F 0 "TP8" H 3050 3100 50  0000 C BNN
F 1 "TEST" H 3050 3050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 3050 2800 50  0001 C CNN
F 3 "" H 3050 2800 50  0000 C CNN
	1    3050 2800
	1    0    0    -1  
$EndComp
$Comp
L TEST TP6
U 1 1 58B28346
P 2650 2800
F 0 "TP6" H 2650 3100 50  0000 C BNN
F 1 "TEST" H 2650 3050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 2650 2800 50  0001 C CNN
F 3 "" H 2650 2800 50  0000 C CNN
	1    2650 2800
	1    0    0    -1  
$EndComp
Text HLabel 2650 2800 3    60   Input ~ 0
WP
Text HLabel 2850 2800 3    60   Input ~ 0
SCL
Text HLabel 3050 2800 3    60   Input ~ 0
SDA
$Comp
L TEST TP30
U 1 1 58B31484
P 2450 2800
F 0 "TP30" H 2450 3100 50  0000 C BNN
F 1 "TEST" H 2450 3050 50  0000 C CNN
F 2 "Measurement_Points:Measurement_Point_Round-TH_Small" H 2450 2800 50  0001 C CNN
F 3 "" H 2450 2800 50  0000 C CNN
	1    2450 2800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR020
U 1 1 58B314B4
P 2450 2800
F 0 "#PWR020" H 2450 2550 50  0001 C CNN
F 1 "GND" H 2450 2650 50  0000 C CNN
F 2 "" H 2450 2800 50  0000 C CNN
F 3 "" H 2450 2800 50  0000 C CNN
	1    2450 2800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
